/*
Importer les composants
*/
  // nodeJS
  require('dotenv').config();
  const express = require('express');
  const path = require('path');

  // Inner
  const frontRouter = require('./routes/front.router');
  const apiRouter = require('./routes/api.router');
//


/*
Configuration du serveur
*/
  // Définir les variables serveur
  const server = express();     // express JS est plus rapide a l'execution et dans le code 
  const port = process.env.PORT;

  // Configuration du moteur de rendu 
  server.set('view engine', 'ejs');

  // Définition du dossier static du client 
  server.set( 'views', __dirname + '/www');
  server.use( express.static(path.join(__dirname, 'www')) );

  // Utilisation des routers
  server.use('/', frontRouter)
//


/*
Lancer le serveur 
// Une fonction callback est une fonction qui est executer à la suite d'une premiere fonction
*/
  server.listen( port, () => console.log(`Server listening on port ${port}`))         //function listen of expressJS 
                                  
//